var Salmon = 375;
var Tuna = 400;
var Catfish = 180;

function add(){

	var item = document.forms.ShoppingList.item.value;
	var quantity = document.forms.ShoppingList.quantity.value;
	
	if(quantity.length == 0){
    	quantity = "0";
	}
	if(quantity > 10){
    	quantity = "10";
	}
	localStorage.setItem(item, quantity);

	doShowAll();
	total()

}

function CheckBrowser() {
    if ('localStorage' in window && window['localStorage'] !== null) {
        // We can use localStorage object to store data.
        return true;
    } else {
            return false;
    }
}


function doShowAll() {
    if (CheckBrowser()) {
        var key = "";
        var list = "<tr><th>Item</th><th>Quantity</th></tr>\n";
        var i = 1;
        var hee = JSON.parse(window.localStorage.getItem('Banana'));
        var KeyName = window.localStorage.key(0)
        //For a more advanced feature, you can set a cap on max items in the cart.
        for (i = 0; i <= localStorage.length-1; i++) {
            key = localStorage.key(i);
            list += "<tr><td>" + key + "</td>\n<td>"
                    + localStorage.getItem(key) + "</td>\n</tr>\n";
        }
        //If no item exists in the cart.
        if (list == "<tr><th>Item</th><th>quantity</th></tr>\n") {
            list += "<tr><td><i>empty</i></td>\n<td><i>empty</i></td></tr>\n";
        }
        //Bind the data to HTML table.
        document.getElementById('list').innerHTML = list;
    } else {
        alert('Cannot save shopping list as your browser does not support HTML 5');
    }
}

function update(){
	var A = item + " " + quantity;
	var B = B + A;
	 item=document.getElementById("item").value;
	 quantity=document.getElementById("quantity").value;

	document.getElementById("pick").innerHTML= B;
}

function SaveItem() {

	var inputName= document.getElementById("name");
	localStorage.setItem("name", inputName.value);

	var inputPhone= document.getElementById("phone");
	localStorage.setItem("phone", inputPhone.value);

	var inputEmail= document.getElementById("email");
	localStorage.setItem("email", inputEmail.value);

	var inputBilling= document.getElementById("billing");
	localStorage.setItem("billing", inputBilling.value);

	var inputShipping= document.getElementById("shipping");
	localStorage.setItem("shipping", inputShipping.value);
	
}

function SaveForm() {

	var inputName= document.getElementById("name");
	localStorage.setItem("name", inputName.value);

	var inputPhone= document.getElementById("phone");
	localStorage.setItem("phone", inputPhone.value);

	var inputEmail= document.getElementById("email");
	localStorage.setItem("email", inputEmail.value);

	var inputBilling= document.getElementById("billing");
	localStorage.setItem("billing", inputBilling.value);

	var inputShipping= document.getElementById("shipping");
	localStorage.setItem("shipping", inputShipping.value);
	
}

function order(){
	var sal = JSON.parse(window.localStorage.getItem('Salmon'));
	var tun = JSON.parse(window.localStorage.getItem('Tuna'));
	var cat = JSON.parse(window.localStorage.getItem('Catfish'));
	if(sal == null){
    	sal = "0";
	}
	if(tun == null){
    	tun = "0";
	}
	if(cat == null){
    	cat = "0";
	}
	var total = ((sal*Salmon) + (tun*Tuna) + (cat*Catfish));
	var total2 = total + 100;
	var name = window.localStorage.getItem('name');
	var phone = window.localStorage.getItem('phone');
	var email = window.localStorage.getItem('email');
	var billing = window.localStorage.getItem('billing');
	var shipping = window.localStorage.getItem('shipping');
	
   
	document.getElementById("name").innerHTML = "Name: " + name;
	document.getElementById("phone").innerHTML = "Phone: " + phone;
	document.getElementById("email").innerHTML = "Email: " + email;
	document.getElementById("billing").innerHTML = "Billing Address: " + billing;
	document.getElementById("shipping").innerHTML = "Shipping Address: " + shipping;
	document.getElementById("subtotal").innerHTML = 'Subtotal <span> ' + total.toFixed(2) + ' ₱ </span>';
	document.getElementById("total").innerHTML = 'Total <span> ' + (total2.toFixed(2)) + ' ₱ </span>';

}

function total() {
	var sal = JSON.parse(window.localStorage.getItem('Salmon'));
	var tun = JSON.parse(window.localStorage.getItem('Tuna'));
	var cat = JSON.parse(window.localStorage.getItem('Catfish'));
	if(sal == null){
    	sal = "0";
	}
	if(tun == null){
    	tun = "0";
	}
	if(cat == null){
    	cat = "0";
	}
	

	document.getElementById("demo2").innerHTML = "Salmon price: 375 &#8369 * " + sal + " " + "total: " + (sal*Salmon);
	document.getElementById("demo3").innerHTML = "Tuna price: 400 &#8369 * " + tun + " " + "total: " + (tun*Tuna);
	document.getElementById("demo4").innerHTML = "Catfish price: 180 &#8369 * " + cat + " " + "total: " + (cat*Catfish);
	document.getElementById("demo").innerHTML = "Sub Total" + " " + ((sal*Salmon) + (tun*Tuna) + (cat*Catfish)) + " &#8369";
  }

  function Clear() {
	window.localStorage.clear();
	total()
	doShowAll();
  }

  function All() {
	doShowAll();
	total()
	
  }